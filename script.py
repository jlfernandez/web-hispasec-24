#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'framirez@hispasec.com'

from jinja2 import Environment, FileSystemLoader
from babel.support import Translations
from jac import CompressorExtension

import os
import sys
# from BeautifulSoup import *
from bs4 import BeautifulSoup
from bs4 import Comment

PATH = os.path.dirname(os.path.abspath(__file__))
PATH_TEMPLATE = PATH + '/templates/'
LANGS = ["es_ES", "en_US"]


def writeTemplate():
    env = Environment(
        extensions=['jinja2.ext.i18n', 'jinja2.ext.autoescape', 'jinja2.ext.with_', CompressorExtension])

    env.compressor_output_dir = './output/static'
    env.compressor_static_prefix = '/static'
    env.compressor_source_dirs = './static'
    env.trim_blocks = True
    env.lstrip_blocks = True

    for (path, ficheros, archivos) in os.walk(PATH_TEMPLATE):
        for file in archivos:
            route_file = (path + "/" + file).replace(PATH_TEMPLATE, "")

            if route_file.startswith("layout"):
                continue
            if not route_file.startswith("/"):
                route_file = "/" + route_file

            # Se genera el index
            for lang in LANGS:
                env.loader = FileSystemLoader(PATH_TEMPLATE)
                env.install_gettext_translations(
                    Translations.load('locale', lang))
                tmpl = env.get_template(route_file)
                # Renderizamos los datos en el template

                lang_code = "es"
                if lang is 'en_US':
                    lang_code = "en"

                web_html = tmpl.render(lang_code=lang_code)

                # Ponemos en html bonito
                soup = BeautifulSoup(web_html, "html.parser")
                # jmesa: delete html comments
                comments = soup.findAll(
                    text=lambda text: isinstance(text, Comment))
                [comment.extract() for comment in comments]
                # web_html = soup.prettify()
                
                template_file = PATH + "/output/" + lang[0:2] + route_file

                # Creamos los directorios y ficheros por cada template
                path_create = '/'.join(str(x)
                                       for x in template_file.split("/")[:-1])
                if not os.path.exists(path_create):
                    os.makedirs(path_create)

                f = open(template_file, "w+")
                f.write(web_html.encode('utf-8'))
                f.close()
        # jmesa: break the loop to only convert the Template folder and not the
        # subdirs
        # break

if __name__ == '__main__':
    writeTemplate()
